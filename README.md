**Generative Summary** is a custom Drupal module designed to simplify content
summarization. With just a click of the "Generate Summary" button, content
creators can leverage the OpenAI Chat Completions API to obtain concise and
meaningful summaries for their content. This module offers a unique blend of
global configurations coupled with field-specific settings, providing granular
control over the summary generation process.

### Features
- **Automatic Summarization**: Integrate with the OpenAI Chat Completions API
for automatic content summarization.
- **User-friendly Interface**: A simple "Generate Summary" button added to the
summary field ensures a smooth user experience.
- **Layered Configuration Approach**: Set global defaults for all text fields
and further customize settings for individual fields with "Generative Summary"
enabled.
- **Configurable Summary Length**: Define both global and field-specific minimum
and maximum lengths for the generated summary.
- **Sentence Limit Configuration**: Set both global and field-specific maximum
number of sentences for the generated summary.
- **Custom Prompting**: Configure global and field-specific custom prompts for
system and user roles, tailoring the context for the OpenAI API.

### Post-Installation
- Users will find a new "Generate Summary" button in the summary field of their
content.
- Navigate to the module's configuration page at
`/admin/config/content/generative_summary` to set up the OpenAI API key and
customize global summary generation settings.
- For field-specific configurations, navigate to the respective field's settings
page.
