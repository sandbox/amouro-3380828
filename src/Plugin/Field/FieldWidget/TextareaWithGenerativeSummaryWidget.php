<?php

namespace Drupal\generative_summary\Plugin\Field\FieldWidget;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'textarea_with_generative_summary' widget.
 *
 * @FieldWidget(
 *   id = "textarea_with_generative_summary",
 *   label = @Translation("Text area with a generative summary"),
 *   field_types = {
 *     "text_with_summary"
 *   }
 * )
 */
class TextareaWithGenerativeSummaryWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'rows' => '9',
      'summary_rows' => '3',
      'placeholder' => '',
      'show_summary' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['rows'] = [
      '#type' => 'number',
      '#title' => $this->t('Rows'),
      '#default_value' => $this->getSetting('rows'),
      '#description' => $this->t('Text editors may override this setting.'),
      '#required' => TRUE,
      '#min' => 1,
    ];
    $element['placeholder'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];
    $element['summary_rows'] = [
      '#type' => 'number',
      '#title' => $this->t('Summary rows'),
      '#default_value' => $this->getSetting('summary_rows'),
      '#description' => $element['rows']['#description'],
      '#required' => TRUE,
      '#min' => 1,
    ];
    $element['show_summary'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Always show the summary field'),
      '#default_value' => $this->getSetting('show_summary'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Number of summary rows: @rows', ['@rows' => $this->getSetting('summary_rows')]);
    if ($this->getSetting('show_summary')) {
      $summary[] = $this->t('Summary field will always be visible');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element += [
      '#base_type' => 'textarea',
      '#format' => $items[$delta]->format,
      '#type' => 'text_format',
      '#default_value' => $items[$delta]->value,
      '#rows' => $this->getSetting('rows'),
      '#placeholder' => $this->getSetting('placeholder'),
      '#attributes' => ['class' => ['js-text-full', 'text-full']],
    ];

    $allowed_formats = $this->getFieldSetting('allowed_formats');

    if ($allowed_formats && !$this->isDefaultValueWidget($form_state)) {
      $element['#allowed_formats'] = $allowed_formats;
    }

    $display_summary = $items[$delta]->summary || $this->getFieldSetting('display_summary');
    $required = empty($form['#type']) && $this->getFieldSetting('required_summary');

    $element['summary'] = [
      '#type' => $display_summary ? 'textarea' : 'value',
      '#default_value' => $items[$delta]->summary,
      '#title' => $this->t('Summary'),
      '#rows' => $this->getSetting('summary_rows'),
      '#description' => !$required ? $this->t('Leave blank to use trimmed value of full text as the summary.') : '',
      '#attributes' => ['class' => ['text-summary']],
      '#prefix' => '<div class="js-text-summary-wrapper text-summary-wrapper">',
      '#suffix' => '</div>',
      '#weight' => -10,
      '#required' => $required,
    ];

    if (!$this->getSetting('show_summary') && !$required) {
      $element['summary']['#attributes']['class'][] = 'js-text-summary';
      $element['summary']['#attached']['library'][] = 'text/drupal.text';
    }

    $field_instance = $items->getFieldDefinition();
    $enabled = $field_instance->getThirdPartySetting('generative_summary', 'generative_summary_enabled');

    if ($enabled) {
      $min_length = $field_instance->getThirdPartySetting('generative_summary', 'min_length');
      $max_length = $field_instance->getThirdPartySetting('generative_summary', 'max_length');
      $max_sentences = $field_instance->getThirdPartySetting('generative_summary', 'max_sentences');
      $max_chunk_tokens = $field_instance->getThirdPartySetting('generative_summary', 'max_chunk_tokens');
      $custom_prompt_system_role = $field_instance->getThirdPartySetting('generative_summary', 'custom_prompt_system_role');
      $custom_prompt_user_role = $field_instance->getThirdPartySetting('generative_summary', 'custom_prompt_user_role');

      $field_name = $field_instance->getName();
      $wrapper_id = 'edit-summary-wrapper-' . $field_name . '-' . $delta;
      $element['summary']['#prefix'] = '<div id="' . $wrapper_id . '" class="js-text-summary-wrapper text-summary-wrapper">';
      $element['summary']['#suffix'] = '</div>';

      $element['generate_summary'] = [
        '#type' => 'button',
        '#name' => 'button-' . $wrapper_id,
        '#value' => $this->t('Generate Summary'),
        '#prefix' => '<div id="button-' . $wrapper_id . '" class="js-text-summary-wrapper text-summary-wrapper">',
        '#suffix' => '<div class="hidden messages"></div></div>',
        '#ajax' => [
          'callback' => [$this, 'ajaxGenerateSummary'],
          'wrapper' => $wrapper_id,
          'effect' => 'fade',
        ],
        '#weight' => -9,
        '#options' => [
          'field_name' => $field_name,
          'delta' => $delta,
          'min_length' => $min_length,
          'max_length' => $max_length,
          'max_sentences' => $max_sentences,
          'max_chunk_tokens' => $max_chunk_tokens,
          'custom_prompt_system_role' => $custom_prompt_system_role,
          'custom_prompt_user_role' => $custom_prompt_user_role,
        ],
      ];
    }

    return $element;
  }

  /**
   * Generates a summary via AJAX.
   *
   * @param array $form
   *   The form array.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return Drupal\Core\Ajax\AjaxResponse
   *   The AJAX response object.
   */
  public static function ajaxGenerateSummary(array $form, FormStateInterface $form_state): AjaxResponse {
    $triggering_element = $form_state->getTriggeringElement();
    $field_name = $triggering_element['#options']['field_name'];
    $delta = $triggering_element['#options']['delta'];

    $field_values = $form_state->getValue($field_name);
    $body = $field_values[0]['value'];

    $min_length = $triggering_element['#options']['min_length'];
    $max_length = $triggering_element['#options']['max_length'];
    $max_sentences = $triggering_element['#options']['max_sentences'];
    $max_chunk_tokens = $triggering_element['#options']['max_chunk_tokens'];
    $custom_prompt_system_role = $triggering_element['#options']['custom_prompt_system_role'];
    $custom_prompt_user_role = $triggering_element['#options']['custom_prompt_user_role'];

    $response = new AjaxResponse();

    if (\strlen($body) < $min_length) {
      $response->addCommand(
        new InvokeCommand(
          \sprintf('#button-edit-summary-wrapper-%s-%d .messages', $field_name, $delta),
          'text',
          [\t('Please write some text before generating a summary. The minimum length is @min_length characters.', ['@min_length' => $min_length])]
        )
      );
      $response->addCommand(
        new InvokeCommand(
          \sprintf('#button-edit-summary-wrapper-%s-%d .messages', $field_name, $delta),
          'removeClass',
          ['hidden']
        )
      );
      $response->addCommand(
        new InvokeCommand(
          \sprintf('#button-edit-summary-wrapper-%s-%d .messages', $field_name, $delta),
          'addClass',
          ['messages--error']
        )
      );

      return $response;
    }
    else {
      $controller = \Drupal::service('generative_summary.controller');
      $summary = $controller->generateSummaryFromText($body, $max_length, $max_sentences, $max_chunk_tokens, $custom_prompt_system_role, $custom_prompt_user_role);

      if ($summary !== '') {
        $response->addCommand(
          new InvokeCommand(
            \sprintf('#button-edit-summary-wrapper-%s-%d .messages', $field_name, $delta),
            'addClass',
            ['hidden']
          )
        );
        $response->addCommand(
          new InvokeCommand(
            \sprintf('#button-edit-summary-wrapper-%s-%d .messages', $field_name, $delta),
            'removeClass',
            ['messages--error']
          )
        );
        $response->addCommand(
          new InvokeCommand(
            \sprintf('#button-edit-summary-wrapper-%s-%d .messages', $field_name, $delta),
            'text',
            ['']
          )
        );
        $response->addCommand(
          new InvokeCommand(
            \sprintf('textarea[name="%s[%d][summary]"]', $field_name, $delta),
            'val',
            [$summary]
          )
        );
      }
      else {
        $response->addCommand(
          new InvokeCommand(
            \sprintf('#button-edit-summary-wrapper-%s-%d .messages', $field_name, $delta),
            'text',
            [\t('Failed generating summary. Please check your settings and the log message.')]
          )
        );
        $response->addCommand(
          new InvokeCommand(
            \sprintf('#button-edit-summary-wrapper-%s-%d .messages', $field_name, $delta),
            'removeClass',
            ['hidden']
          )
        );
        $response->addCommand(
          new InvokeCommand(
            \sprintf('#button-edit-summary-wrapper-%s-%d .messages', $field_name, $delta),
            'addClass',
            ['messages--error']
          )
        );
      }
    }

    return $response;
  }

}
